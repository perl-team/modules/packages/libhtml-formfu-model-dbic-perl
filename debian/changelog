libhtml-formfu-model-dbic-perl (2.03-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Remove constraints unnecessary since stretch:
    + Build-Depends-Indep: Drop versioned constraint on libhtml-formfu-perl,
      libmoosex-attribute-chained-perl, perl, libtest-simple-perl.
    + libhtml-formfu-model-dbic-perl: Drop versioned constraint on
      libhtml-formfu-perl, libmoosex-attribute-chained-perl in Depends.
  * Bump debhelper from old 12 to 13.
  * Update standards version to 4.1.5, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 12:16:50 +0000

libhtml-formfu-model-dbic-perl (2.03-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 23:46:32 +0100

libhtml-formfu-model-dbic-perl (2.03-2) unstable; urgency=medium

  * Add missing version on MooseX::Attribute::Chained dependency

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 May 2018 21:15:46 +0200

libhtml-formfu-model-dbic-perl (2.03-1) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Brian Cassidy from Uploaders. Thanks for your work!

  [ Florian Schlichting ]
  * Add upstream metadata
  * New upstream release: 2.02
  * Update copyright years
  * Drop whatis-entries.diff, applied upstream

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Florian Schlichting ]
  * Import upstream version 2.03
  * Update upstream metadata
  * Refresh how-to-report-bugs.diff (offset)
  * Declare compliance with Debian Policy 4.1.4
  * Re-add libmoosex-attribute-chained-perl dependency

 -- Florian Schlichting <fsfs@debian.org>  Mon, 14 May 2018 16:31:15 +0200

libhtml-formfu-model-dbic-perl (2.00-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Mark package as autopkg-testable
  * Claim conformance with policy 3.9.7
  * Bump debhelper compatibility level to 9

 -- Damyan Ivanov <dmn@debian.org>  Mon, 22 Feb 2016 07:23:55 +0000

libhtml-formfu-model-dbic-perl (2.00-1) unstable; urgency=medium

  * New upstream release.
  * Update versioned (build) dependencies.
  * Drop (build) dependency on libmoosex-attribute-chained-perl.
    Upstream switched from MooseX::Attribute::Chained to
    MooseX::Attribute::FormFuChained (in libhtml-formfu-perl).

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 May 2014 20:44:15 +0200

libhtml-formfu-model-dbic-perl (1.02-2) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Add (build-)dependency on libmoosex-attribute-chained-perl
    (closes: #746062)

 -- Florian Schlichting <fsfs@debian.org>  Fri, 02 May 2014 00:47:19 +0200

libhtml-formfu-model-dbic-perl (1.02-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches (offset).
  * Drop build dependency on libtest-aggregate-perl.

 -- gregor herrmann <gregoa@debian.org>  Mon, 17 Feb 2014 20:47:56 +0100

libhtml-formfu-model-dbic-perl (1.00-1) unstable; urgency=medium

  * New upstream release.
  * Refresh patches (offset).
  * Update years of copyright; drop info about removed files.
  * Update (build) dependencies.
  * Declare compliance with Debian Policy 3.9.5.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2013 22:15:41 +0100

libhtml-formfu-model-dbic-perl (0.09010-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * Fix grammar error in package description. Thanks to Clayton Casciato
    for the bug report and the patch. (Closes: #687468)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.09010
  * Bump years on included M::I copyright
  * Bump dependency on libhtml-formfu-perl to 0.09010
  * Bump Standards-Version to 3.9.4 (update to copyright-format 1.0)
  * Bump dh compatibility to level 8 (no changes necessary)
  * Refresh and forward patches
  * Reformat long line in extended description (lintian warning)
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Tue, 20 Aug 2013 23:04:55 +0200

libhtml-formfu-model-dbic-perl (0.09002-1) unstable; urgency=low

  * New upstream release.
  * Bump (build-)dep on libhtml-formfu-perl to >= 0.09001.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 16 Jul 2011 09:35:37 +0200

libhtml-formfu-model-dbic-perl (0.09000-1) unstable; urgency=low

  * New upstream release.
  * Bump (build-)dep on libhtml-formfu-perl to >= 0.09000.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 24 Apr 2011 12:10:33 +0200

libhtml-formfu-model-dbic-perl (0.08002-1) unstable; urgency=low

  * New upstream release.
  * Add (build-)dep on libyaml-syck-perl.
  * Drop patch fix-pod-errors.patch: fixed upstream.
  * debian/copyright: Formatting changes; update years of copyright; refer to
    /usr/share/common-licenses/GPL-1.
  * Use source format 3.0 (quilt); drop quilt framework and README.source.
  * debian/NEWS: Add note about incompatible upstream changes.
  * Add whatis entry to new Perl module.
    + new patch: whatis-entries.diff
  * Update my email address.
  * Bump Standards-Version to 3.9.1.

 -- Ansgar Burchardt <ansgar@debian.org>  Tue, 12 Oct 2010 18:14:39 +0200

libhtml-formfu-model-dbic-perl (0.06000-1) unstable; urgency=low

  * New upstream release.
    + update (build-)dep on libdbix-class-perl to >= 0.08108.
    + update (build-)dep on libhtml-formfu-perl to >= 0.06000
      to make tests pass and silence warnings.
    + add (build-)dep on libsql-translator-perl.
    + refresh patches.
  * Use minimal debian/rules.
  * Make (build-)dep on perl unversioned as allowed by Policy 3.8.3.
  * Bump Standards-Version to 3.8.3.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 12 Dec 2009 18:24:11 +0900

libhtml-formfu-model-dbic-perl (0.05002-1) unstable; urgency=low

  * New upstream release.
  * debian/control: adjust build and runtime dependencies to upstream
    requirements.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Mon, 13 Jul 2009 01:43:50 +0200

libhtml-formfu-model-dbic-perl (0.05000-2) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Ansgar Burchardt ]
  * Add build-dep on libdatetime-format-sqlite-perl (Closes: #534011).
  * Bump Standards Version to 3.8.2 (no changes).

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 21 Jun 2009 20:06:41 +0200

libhtml-formfu-model-dbic-perl (0.05000-1) unstable; urgency=low

  * New upstream release.
    + Bump libhtml-formfu-perl dependency to 0.05000.
  * Refresh patches.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 27 May 2009 18:11:31 +0200

libhtml-formfu-model-dbic-perl (0.04003-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.

  [ gregor herrmann ]
  * debian/copyright: bump years of copyright for inc/*.

 -- Ansgar Burchardt <ansgar@43-1.org>  Sun, 17 May 2009 16:30:28 +0200

libhtml-formfu-model-dbic-perl (0.04002-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
    + Depends on liblist-moreutils-perl
    + Depends on libhtml-formfu-perl (>= 0.04002): test suite does not pass
      with older versions.
  * Bump Standards Version to 3.8.1 (no changes).
  * Fix POD errors.
    + new patch fix-pod-errors.patch

  [ gregor herrmann ]
  * debian/control:
    - fix Vcs-Browser field
    - make short description a noun phrase

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 09 May 2009 19:59:41 +0200

libhtml-formfu-model-dbic-perl (0.03007-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * add README.source to document quilt usage

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).

  [ Brian Cassidy ]
  * New upstream version
  * debian/control:
    + Added myself to Uploaders
    + Bump libhtml-formfu-perl dep to 0.03007
  * debian/patches: add a description and refresh patch

 -- Brian Cassidy <brian.cassidy@gmail.com>  Mon, 08 Dec 2008 12:45:41 -0400

libhtml-formfu-model-dbic-perl (0.03003-1) unstable; urgency=low

  * Initial Release. (Closes: #495942)

 -- Ansgar Burchardt <ansgar@43-1.org>  Thu, 18 Sep 2008 17:58:15 +0200
